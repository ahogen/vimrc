
require('v9.options')
-- require('v9.keymaps')
require('v9.lazy')
-- require('v1.colorscheme')


vim.o.background = 'dark'
vim.opt.termguicolors = true
vim.cmd("colorscheme vscode")
