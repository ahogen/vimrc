local opts = { noremap = true, silent = true }
local keymap = vim.keymap
local global = vim.g

global.mapleader = ' '
global.maplocalleader = ' '

-- "the greatest remap ever (Primeagen)"
-- replace currently selected text with default register
-- without yanking it
keymap.set("x", "<leader>p", [["_dP]])

-- prevent incrementing numbers in file (this is actually horrible)
keymap.set('v', '<C-a>', 'ggVG', opts)