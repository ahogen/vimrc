vim.cmd("filetype plugin on")
--vim.cmd("filetype indent off")

--vim.opt.number = true
--vim.opt.relativenumber = true

--vim.opt.swapfile = false
--vim.opt.backup = false
vim.opt.undodir = vim.env.HOME .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.spelllang = 'en_us'
vim.opt.spell = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"

vim.opt.tabstop = 8
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.opt.colorcolumn = "80"

-- On Linux Mint Cinnamon, I ran `sudo apt install xsel` to enable neovim clipboard
-- support. Check if enabled with `: checkhealth`
-- The below will cause all "yanks" to yank into the system clipboard.
vim.opt.clipboard:append("unnamedplus")
