local config = function()
    local telescope = require("telescope")
    local builtin = require("telescope.builtin")


    telescope.setup({
        defaults = {
            vimgrep_arguments = { "rg", "--color=never", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case", "--hidden" },
        },
        pickers = {
            find_files = {
                theme = 'dropdown',
                previewer = false,
                hidden = true
            },
            live_grep = {
                additional_args = { "--hidden" }
            }
        },
    })

    vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = "find files" })
    vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = "live grep string" })
    vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = "find buffer" })
    vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = "search (neo)vim help" })
end


return {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.8",
    dependencies = {
        "nvim-lua/plenary.nvim"
    },
    config = config
}
