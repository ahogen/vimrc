return { {
    "lewis6991/gitsigns.nvim",
    tag = "v0.9.0",
    lazy = true,
    event = { 'BufReadPre' },
    opts = {
        signcolumn = true,
        attach_to_untracked = false,
        on_attach = function(bufnr)
            local gs = package.loaded.gitsigns
            local opts = { buffer = bufnr }



            vim.keymap.set('n', ']c', function()
                if vim.wo.diff then return ']c' end
                vim.schedule(function() gs.next_hunk() end)
                return '<Ignore>'
            end, { buffer = bufnr, expr = true }
            )

            vim.keymap.set('n', '[c', function()
                if vim.wo.diff then return ']c' end
                vim.schedule(function() gs.prev_hunk() end)
                return '<Ignore>'
            end, { buffer = bufnr, expr = true }
            )

            vim.keymap.set('n', '<leader>gs', gs.preview_hunk, opts)
            vim.keymap.set('n', '<leader>gb', function() gs.blame_line{full=true} end, opts)
        end
    }

} }
