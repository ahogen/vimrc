return {{
    'nvim-treesitter/nvim-treesitter',
    event = { 'BufReadPre', 'BufNewFile' },
    cmd = { 'TSInstallInfo', 'TSInstall' },
    build = ":TSUpdate",
    config = function()
        local status_ok, treesitter = pcall(require, 'nvim-treesitter.configs')

        if not status_ok then
            return
        end

        treesitter.setup({
            ensure_installed = { 'bash', 'c', 'cpp', 'cmake', 'devicetree', 'dockerfile', 'gitattributes', 'gitcommit', 'gitignore', 'json', 'kconfig', 'lua', 'make', 'markdown_inline', 'python', 'rust', 'toml', 'vimdoc', 'yaml' },
            sync_install = false,
            auto_install = false,
            ignore_install = {},
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
            context_commentstring = {
                enable = true,
                autocmd = false
            },
            autopairs = {
                enable = true
            },
            autotag = {
                enable = true
            },
            indent = {
                enable = false,
                disable = { 'go' }
            }
        })
    end
}}