return {

    -- LSP Support
    {
        'neovim/nvim-lspconfig',
        lazy = true,
        event = { 'BufReadPre', 'BufNewFile' },

        dependencies = {
            { 'williamboman/mason.nvim',           lazy = true },
            { 'williamboman/mason-lspconfig.nvim', lazy = true },
            { 'ray-x/lsp_signature.nvim',          lazy = true },
            { 'hrsh7th/nvim-cmp',                  lazy = true,
                dependencies = { 'hrsh7th/vim-vsnip', 'hrsh7th/cmp-vsnip',  'hrsh7th/cmp-nvim-lsp'} ,
            },
        },
        config = function()
            require('mason').setup({})
            local mason_lspconfig = require('mason-lspconfig')
            mason_lspconfig.setup({
                ensure_installed = {
                    "clangd",
                    "lua_ls",
                    -- "autotools_ls",         -- Autoconf, Automake, Make
                    "bashls",
                    -- "jedi_language_server", -- Python
                    "ruff", -- Python
                    "rust_analyzer",
                    -- "harper_ls" -- Grammerly for developers!
                }
            })

            mason_lspconfig.setup_handlers {
                function(server_name)
                    --local lspconfig = require('lspconfig')

                    vim.api.nvim_create_autocmd('LspAttach', {
                        desc = 'LSP actions',
                        callback = function(event)
                            local opts = { buffer = event.buf, remap = false }
                            vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
                            vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
                            vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
                            vim.keymap.set("n", "<leader>ca", function() vim.lsp.buf.code_action() end, opts)

                            require("lsp_signature").on_attach({
                                bind = true,
                                handler_opts = {
                                    border = "rounded",
                                },
                                doc_lines = 0,
                            }, event.bufnr)

                            -- Use LSP to format this file
                            vim.keymap.set('n', '<leader>f', function()
                                vim.lsp.buf.format { async = true }
                            end, opts)

                            local cmp = require('cmp')
                            cmp.setup({
                                snippet = {
                                    expand = function(args)
                                        vim.fn["vsnip#anonymous"](args.body)
                                    end,
                                },
                                sources = {
                                    {name = 'nvim_lsp'},
                                },
                                window = {
                                    completion = cmp.config.window.bordered(),
                                    documentation = cmp.config.window.bordered()
                                }
                            })

                            local lspconfig = require('lspconfig')
                            lspconfig[server_name].setup({
                                capabilities = require('cmp_nvim_lsp').default_capabilities()

                            })

                        end
                    })
                end
                ,
                clangd = function()
                    -- Whitelist Cortex-M4 toolchain path.
                    local clangd_query_driver = ''
                    if os.getenv("GCC_ARM_NONE_EABI_DIR") then
                        clangd_query_driver = clangd_query_driver ..
                            (os.getenv("GCC_ARM_NONE_EABI_DIR") .. '/bin/*,')
                    elseif os.getenv("GCC_ARM_NONE_PATH") then
                        clangd_query_driver = clangd_query_driver .. (os.getenv("GCC_ARM_NONE_PATH") .. '/bin/*,')
                    end

                    if clangd_query_driver == '' then
                        clangd_query_driver = nil
                    else
                        clangd_query_driver = '--query-driver=' .. clangd_query_driver
                    end

                    local lspconfig = require('lspconfig')

                    lspconfig.clangd.setup {
                        cmd = { 'clangd', '-j=4', clangd_query_driver, '--enable-config' },
                        capabilities = require('cmp_nvim_lsp').default_capabilities()
                    }
                end,
                autotools_ls = function()
                    require("lspconfig").autotools_ls.setup {}
                end,
                bashls = function()
                    require("lspconfig").bashls.setup {}
                end,
                --jedi_language_server = function()
                --    require("lspconfig").jedi_language_server.setup {}
                --end,
                lua_ls = function()
                    require('lspconfig').lua_ls.setup {}
                end,
                ruff = function()
                    require('lspconfig').ruff.setup {}
                end,
                --pyright = function()
                --    require('lspconfig').pyright.setup {}
                --end,
                --harper_ls = function()
                --    require('lspconfig').harper_ls.setup {
                --      settings = {
                --        ["harper-ls"] = {
                --          linters = {
                --            spell_check = true,
                --            spelled_numbers = false,
                --            an_a = true,
                --            sentence_capitalization = true,
                --            unclosed_quotes = true,
                --            wrong_quotes = false,
                --            long_sentences = true,
                --            repeated_words = true,
                --            spaces = true,
                --            matcher = true,
                --            correct_number_suffix = true,
                --            number_suffix_capitalization = true,
                --            multiple_sequential_pronouns = true
                --          }
                --        }
                --      },
                --    }
                --end,
            }
        end
    },
}
