#!/usr/bin/env bash

#
# TODO: Utilize GNU Stow to create symlinks of my dotfiles.
# https://www.gnu.org/software/stow/manual/stow.html
#

set -eo pipefail
set -u
set -x

NVIM_VERSION=0.10.1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    wget \
    xsel \
    ripgrep \
    shellcheck \
    build-essential \
    python3-pip \
    python3-venv \
    libfuse2

rm -v "${HOME}/AppImage/nvim.appimage" || true
rm -rf \
    "${HOME}/.local/share/nvim" \
    "${HOME}/.local/state/lvim" \
    "${HOME}/.local/state/nvim"

if [ ! -L "${HOME}/.config/nvim" ]; then
    rm -rf "${HOME}/.config/nvim"
else
    unlink "${HOME}/.config/nvim"
fi

mkdir -p "${HOME}/AppImage"

# Download and install Neovim
wget https://github.com/neovim/neovim/releases/download/v${NVIM_VERSION}/nvim.appimage -O "${HOME}/AppImage/nvim.appimage"
sync
echo 'c4762d54cadfd9fa4497c7969197802c9cf9e0d926c39e561f0bd170e36c8aa0  nvim.appimage' > "${HOME}/AppImage/nvim.appimage.sha256sum"
pushd "${HOME}/AppImage"
    sha256sum --check "nvim.appimage.sha256sum"
popd
rm "${HOME}/AppImage/nvim.appimage.sha256sum"
chmod u+x "${HOME}/AppImage/nvim.appimage"

mkdir -p "${HOME}/.local/bin"
ln --symbolic --verbose --force "${HOME}/AppImage/nvim.appimage" "${HOME}/.local/bin/nvim"
ln --symbolic --verbose --force "${SCRIPT_DIR}" "${HOME}/.config/nvim"

# Download a font I like: "Iosevka Comfy Wide Fixed Expanded"
pushd ~/git
if [ ! -d ./iosevka-comfy ]; then
    git clone --filter=tree:0 --no-checkout --depth 1 --sparse https://github.com/protesilaos/iosevka-comfy.git ./iosevka-comfy
fi
pushd ./iosevka-comfy
git clean -fdx # clean '.uuid' files created by linux font tools
git sparse-checkout set iosevka-comfy-wide-fixed/TTF/
git checkout
git pull

ln --symbolic --verbose --force "${PWD}/iosevka-comfy-wide-fixed/TTF" "${HOME}/.local/share/fonts/iosevka-comfy-wide-fixed"
fc-cache ~/.local/share/fonts
popd
popd

# Install NodeJS with NVM
#
# While I don't write JavaScript nonsense, a bunch of LSPs or linters have been
# written in JS, I think due to the popularity of VSCode.
if [ ! -d "${HOME}/.nvm" ]; then
    wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
fi

set +x
if command -v nvm &> /dev/null; then
    echo "nvm exists"
else
    . "${HOME}/.nvm/nvm.sh"
fi

echo "Installing NodeJS with NVM..."
nvm install node

echo "Installing Rust using Cargo..."
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
